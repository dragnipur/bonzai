# Build
FROM node:8 as app-build
COPY . /app
WORKDIR /app
RUN yarn
RUN yarn install && yarn lint && yarn build

# Nginx
FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=app-build /app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
EXPOSE 8080