export abstract class StoreShardUtil {
  public static getFromStorage(key: string, defaultValue: any): any {
    const val = localStorage.getItem(key);
    return val ? JSON.parse(val) : defaultValue;
  }

  public static putInStorage(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
}
