import Vue from 'vue';

import { Store } from '@/model/Store';
import { BaseState } from '@/store';
import { Module } from 'vuex';

export interface StoreCommonState {
  modalOpen: boolean;
  modalsOpen: any;
  activeRoute: string;
}

export const commonShard: Module<StoreCommonState, BaseState> = {
  state: {
    modalOpen: false,
    modalsOpen: {},
    activeRoute: '',
  },
  mutations: {
    toggleModal(state, modalName) {
      const open = !state.modalsOpen[modalName];
      Vue.set(state.modalsOpen, modalName, open);
      state.modalOpen = open;
    },

    closeAllModals(state) {
      for (const key of Object.keys(state.modalsOpen)) {
        Vue.set(state.modalsOpen, key, false);
      }
      state.modalOpen = false;
    },

    setActiveRoute(state, route: string) {
      state.activeRoute = route;
    },
  },
};
