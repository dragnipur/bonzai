import { Recipe } from '@/model/Recipe';
import { BaseState } from '@/store';
import { Module } from 'vuex';
import { StoreShardUtil } from '@/store/shards/storeShardUtil';

export interface StoreRecipesState {
  recipes: Recipe[];
}

export const recipesShard: Module<StoreRecipesState, BaseState> = {
  state: {
    recipes: StoreShardUtil.getFromStorage('recipes', []),
  },

  mutations: {
    addRecipe(state, recipe: Recipe) {
      state.recipes.push(recipe);
      StoreShardUtil.putInStorage('recipes', state.recipes);
    },
  },
};
