import Vuex, { Module } from 'vuex';
import { Store } from '@/model/Store';
import { StoreShardUtil } from '@/store/shards/storeShardUtil';
import uuid from 'uuid';
import { Item } from '@/model/Item';
import { BaseState } from '@/store';
import { Offer } from '@/model/Offer';

export interface StoreState {
  stores: Store[];
  activeStore: Store | null;
  selectedStores: Store[];
  offers: Offer[];
}

export const storeShard: Module<StoreState, BaseState> = {
  state: {
    stores: [],
    selectedStores: StoreShardUtil.getFromStorage('selectedStores', []),
    activeStore: null,
    offers: [],
  },
  mutations: {
    setStores(state, stores: Store[]) {
      state.stores = stores;
    },

    setActiveStore(state, store: Store) {
      state.activeStore = store;
    },

    selectStore(state, store) {
      state.selectedStores.push({
        _id: store.value,
        title: store.label,
        items: [],
      });
      StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
    },

    unselectStore(state, store: Store) {
      const index = state.selectedStores.indexOf(store);
      if (index > -1) state.selectedStores.splice(index, 1);
      StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
    },

    addItem(state: StoreState, title: string): void {
      if (state.activeStore) {
        state.activeStore.items.push({
          id: uuid.v1(),
          title,
          done: false,
        });
        StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
      }
    },

    removeItem(state: StoreState, id: string): void {
      if (state.activeStore) {
        const index = state.activeStore.items.findIndex(entry => entry.id === id);
        const item = state.activeStore.items[index];

        if (index > -1) {
          state.activeStore.items.splice(index, 1);
        }
        StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
      }
    },

    toggleItem(state: StoreState, id: string) {
      if (state.activeStore) {
        const item = state.activeStore.items.find(entry => entry.id === id);
        if (item) item.done = !item.done;
        StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
      }
    },

    clearActiveList(state: StoreState) {
      if (state.activeStore) {
        state.activeStore.items = [];
        StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
      }
    },

    clearAllLists(state: StoreState) {
      state.selectedStores = state.selectedStores.map(store => {
        store.items = [];
        return store;
      });
      StoreShardUtil.putInStorage('selectedStores', state.selectedStores);
    },

    setOffers(state, offers) {
      state.offers = offers;
    },
  },
  actions: {
    fetchStores: async context => {
      const stores = [
        { _id: '1', title: 'Albert Heijn', items: [] },
        { _id: '2', title: 'Aldi', items: [] },
        { _id: '3', title: 'Action', items: [] },
      ];
      context.commit('setStores', stores);
    },
    fetchActiveStoreOffers: async context => {
      if (context.state.activeStore) {
        const offersResponse = await fetch(`/api/offers/${context.state.activeStore._id}`);
        const offers = await offersResponse.json();
        context.commit('setOffers', offers);
      }
    },
  },
};
