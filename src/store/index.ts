import Vue from 'vue';
import Vuex from 'vuex';
import { Store } from '@/model/Store';
import { commonShard } from '@/store/shards/commonShard';
import { storeShard } from '@/store/shards/storeShard';
import { recipesShard } from '@/store/shards/RecipesShard';

Vue.use(Vuex);

// tslint:disable-next-line:no-empty-interface
export interface BaseState {
  activeStore: Store;
}

export default new Vuex.Store<any>({
  modules: {
    common: commonShard,
    store: storeShard,
    recipes: recipesShard,
  },
});
