import { Vue } from 'vue-property-decorator';
export const FocusOnCreation = {
  bind(el: HTMLElement) {
    Vue.nextTick(() => {
      el.focus();
    });
  },
};
