import { Ingredient } from '@/model/Ingredient';
import { Step } from '@/model/Step';

export interface Recipe {
  title: string;
  description: string;
  timeInMinutes: number;
  logo: string;
  ingredients: Ingredient[];
  steps: Step[];
}
