import { Item } from '@/model/Item';

export interface Store {
  _id: string;
  title: string;
  items: Item[];
}
