export interface Offer {
  _id: string;
  title: string;
  store: string;
  discountLabel: string;
  oldPrice: number;
  newPrice: number;
}
