import Vue from 'vue';
import Router from 'vue-router';
import BzList from '@/components/list/BzList.vue';
import BzOffers from '@/components/offers/BzOffers.vue';
import BzStores from '@/components/list/BzStores.vue';
import BzRecipes from '@/components/recipes/BzRecipes.vue';
import BzAddRecipe from '@/components/recipes/BzAddRecipe.vue';
import BzRecipeDetails from '@/components/recipes/BzRecipeDetails.vue';
import store from '@/store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'list',
      component: BzList,
      beforeEnter: (to, from, next) => {
        if (store.state.store.selectedStores.length === 0) {
          next('/winkels');
        } else {
          next();
        }
      },
    },
    {
      path: '/aanbiedingen',
      name: 'offers',
      component: BzOffers,
      beforeEnter: (to, from, next) => {
        if (store.state.store.selectedStores.length === 0) {
          next('/winkels');
        } else {
          next();
        }
      },
    },
    {
      path: '/winkels',
      name: 'stores',
      component: BzStores,
    },
    {
      path: '/recepten',
      name: 'recipes',
      component: BzRecipes,
    },
    {
      path: '/recepten/nieuw',
      name: 'newRecipe',
      component: BzAddRecipe,
    },
    {
      path: '/recepten/:title/:index',
      name: 'recipeDetails',
      component: BzRecipeDetails,
    },
  ],
});

router.beforeEach((to, from, next) => {
  store.commit('setActiveRoute', to.name);
  next();
});

export default router;
